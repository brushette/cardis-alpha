# Cardis version Alpha

#### Version alpha du serveur Minecraft RP Cardis ####


Le serveur Cardis est un serveur Minecraft RP de type fantasy / médiéval.

Il n'est actuellement pas encore accessible mais nous travaillons sur une version alpha qui nous permettra de prendre vos premiers retours.


En attendant son ouverture vous pouvez trouver :

* le wiki et fonctionnalités ici : TODO

* les développements et configurations prévus ici : TODO

* un tableau pour apporter vos suggestions ici : TODO


### Recrutement ###

Nous recherchons des personnes fiables pour nous aider dans notre projet :

* Un quest master qui pourra écrire différentes quêtes

* Un quest maker qui pourra configurer les quêtes (plugin utilisé: BetonQuest)

* Des builder pour construire de jolies villes et donjon :D

* Un community manager pour animer le compte twitter et discord

* Un blogueur pour alimenter le blog d'articles en rapport avec le serveur et Minecraft en général

* Un designer 3D (pour le resource pack)
